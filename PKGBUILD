clear

pkgbase=linux-prevail
pkgver=6.7.5
pkgrel=1
pkgdesc='Linux Prevail'
url='https://github.com/zen-kernel/zen-kernel'
arch=(x86_64)
license=(GPL2)

echo -e '\e[1;38;5;214mPREVAIL KERNEL BUILDER:' "\e[1;38;5;161mv${pkgver}\e[0m"
echo -e '\e[1;38;5;93m''
                                      ████████████                             
                                    █████████████████                          
            █████████               ████        ██████        ███████          
          █████████████             █████          █████  █████████████        
        ██████     █████████  ██████████████████    ████████████   ████        
      █████         ████████████████████   ██████  █  █████████    ████        
    █████      ███            █████         ████   █    ██   ██    ████        
   █████    ██████   █         █           █████  █     ███████    ████        
  ████     ███████  ██  ████  ██  ██████   ████  ██     ███████   █████        
 ████    ████████   ██  ██  ████     ███   ████  ██   █ ██   ███  ████         
 ███     ███████   ████       █      ███   ███  ██   ██  █   ██   ████         
 ████     █████          ██   █  ████   █   ██  ██  ███  ██  ██  ████          
  ██████  ████     ████  ██   █  ████   █   ██  █        ██  ██  ████████████  
    ████  ██   ███████   ██   █  ████  ██   █       ████  █  ██  █████████████ 
    ████    ██████████   █   ██        ██   █      ██████       ███████    ████
    ████   █████  ████   █   █████████████    ██  ████████   █   ██████    ████
    ████   ████   ████████   █████████████    ███████ ██████████   █████   ████
    ████    ████    █████    █████    ████    █████     █████████    ███   ████
    ████    ████     ████    ████     ████   ████            █████         ████
    ████    ████     █████  ████      ████   ████              █████       ████
    █████   ████      ██████████       █████████                ██████   ██████
     ███████████         ████            █████                    ███████████  
       ███████                                                      ███████'

for i in {5..1}
do 
  if [ $((i%1)) == 0 ]
  then 
    echo -e -n "\e[1;38;5;81m$i..."
  fi
  sleep 1
done

echo -e "\e[0m"

clear

echo -e "\e[1;38;5;81mDOWNLOADING SOURCES...\e[0m"

makedepends=(
  bc
  cpio
  gettext
  libelf
  pahole
  perl
  python
  tar
  xz

  # htmldocs
  graphviz
  imagemagick
  python-sphinx
  texlive-latexextra
)
options=('!strip')
_srcname=linux-${pkgver}
_srctag=v${pkgver}
source=(
  https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/${_srcname}.tar.{xz,sign}
  $url/releases/download/$_srctag-zen1/linux-$_srctag-zen1.patch.zst{,.sig}
  config
  prevail-patches.tar.xz
)
validpgpkeys=(
  ABAF11C65A2970B130ABE3C479BE3E4300411886  # Linus Torvalds
  647F28654894E3BD457199BE38DBBDC86092693E  # Greg Kroah-Hartman
  83BC8889351B5DEBBB68416EB8AC08600F108CDF  # Jan Alexander Steffens (heftig)
)
# https://www.kernel.org/pub/linux/kernel/v6.x/sha256sums.asc
sha256sums=('29f6464061b8179cbb77fc5591e06a2199324e018c9ed730ca3e6dfb145539ff'      # linux-6.x.tar.xz
            'SKIP'
            '179467323621ca4e5770a41761446dc176fe7a5ca461da8d2c4114d63d137bbf'      # linux-v6.x-zen1.patch.zst
            'SKIP'
            '87ff4d23f1dbc723409b2278d5e0e91522faa121b315a45a84f219543907b15b'      # config
            '12ab9ab3f02ef1b401a33e9b3045937363ca63d732a9e67f7281dda277c8fab4')     # prevail-patches.tar.xz
b2sums=('91e5abb3905ba9e8b5cdf26b89758f4454b4e573f148fb08340c60852115d95068e44420d73373a406cb47fb011fc14ee65294489f197a3f7f39d3d8e24b2f2d'      # linux-6.x.tar.xz
        'SKIP'
        'a6c85d0fc378e6b89a99213aa31d0721262491b738d61f395d982d401146383c1e4454a94d608a61893e5e15c4813323e34180ea20a92ccc741e180ac670afc2'      # linux-v6.x-zen1.patch.zst
        'SKIP'
        '35307ac69d7060c4bb60b29953bdbabdf963cce73d45979617390f566ad5ba346193f740822a1063c66792f6262707b61d23b592e68f7b74a55c43d011bf257a'      # config
        'a2e3ea44bafddfb3e7d0bf5f21432d6ee4cfa6aa0471905066e474730358daa6b905178bcf44f5b6aa664e252cab15a7555ee74fe4882eaa686a9ba479cbcd6b')     # prevail-patches.tar.xz

export KBUILD_BUILD_HOST=Prevail
export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

prepare() {
  cd $_srcname

  echo "Setting version..."
  echo "" > localversion.10-pkgrel
  echo "" > localversion.20-pkgname
  
  clear

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    src="${src%.zst}"
    [[ $src = *.patch ]] || continue
    # ZEN PATCH
    echo -e "\e[1;38;5;33mAPPLYING ZEN PATCH:\e[0m"
    patch -Np1 < "../$src"

    # BASE PATCHES
    clear
    patch -Np1 < "../patches-$pkgver/base/0001-makefile-prevail.patch"
    patch -Np1 < "../patches-$pkgver/base/0002-kconfig.patch"

    # PERFORMANCE PATCHES
    clear
    patch -Np1 < "../patches-$pkgver/base/0003-fair-set-scheduler.patch"
    patch -Np1 < "../patches-$pkgver/base/0004-block-mq-deadline-Increase-write-priority-to-.patch"
    patch -Np1 < "../patches-$pkgver/base/0005-block-mq-deadline-Disable-front_merges-by-def.patch"
    patch -Np1 < "../patches-$pkgver/base/0006-mm-vmscan-vm_swappiness-30-decreases-the-amou.patch"

    # ANDROID PATCHES
    clear
    patch -Np1 < "../patches-$pkgver/binder/0001-binder-module.patch"
    patch -Np1 < "../patches-$pkgver/binder/0002-binder-mask-file.patch"
    patch -Np1 < "../patches-$pkgver/binder/0003-GPL-module.patch"

    # XANMOD NETWORK PATCHES 
    clear
    patch -Np1 < "../patches-$pkgver/network/0001-tcp-processing.patch"
    patch -Np1 < "../patches-$pkgver/network/0002-netfilter-Add-netfilter-nf_tables-fullcone-support.patch"
    patch -Np1 < "../patches-$pkgver/network/0003-netfilter-add-xt_FLOWOFFLOAD-target.patch"

    # WINE PATCHES
    clear
    patch -Np1 < "../patches-$pkgver/wine/0001-futex.patch"
    patch -Np1 < "../patches-$pkgver/wine/0002-winesync.patch"
    clear

  done

  echo "Setting config..."
  cp ../config .config
  make olddefconfig
  diff -u ../config .config || :

  make -s kernelrelease > version
  echo "Prepared $pkgbase version $(<version)"
}

build() {
  cd $_srcname
  make all
  make htmldocs
}

_package() {
  pkgdesc="The $pkgdesc kernel and modules"
  depends=(
    coreutils
    initramfs
    kmod
  )
  optdepends=(
    'wireless-regdb: to set the correct wireless channels of your country'
    'linux-firmware: firmware images needed for some devices'
  )
  provides=(
    KSMBD-MODULE
    UKSMD-BUILTIN
    VHBA-MODULE
    VIRTUALBOX-GUEST-MODULES
    WIREGUARD-MODULE
  )
  replaces=(
  )

  cd $_srcname
  local modulesdir="$pkgdir/usr/lib/modules/$(<version)"

  echo "Installing boot image..."
  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  echo "Installing modules..."
  ZSTD_CLEVEL=19 make INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 \
    DEPMOD=/doesnt/exist modules_install  # Suppress depmod

  # remove build link
  rm "$modulesdir"/build
}

_package-headers() {
  pkgdesc="Headers and scripts for building modules for the $pkgdesc kernel"
  depends=(pahole)

  cd $_srcname
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map \
    localversion.* version vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile
  cp -t "$builddir" -a scripts

  # required when STACK_VALIDATION is enabled
  install -Dt "$builddir/tools/objtool" tools/objtool/objtool

  # required when DEBUG_INFO_BTF_MODULES is enabled
  install -Dt "$builddir/tools/bpf/resolve_btfids" tools/bpf/resolve_btfids/resolve_btfids

  echo "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/x86" -a arch/x86/include
  install -Dt "$builddir/arch/x86/kernel" -m644 arch/x86/kernel/asm-offsets.s

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */x86/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -Sib "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Stripping vmlinux..."
  strip -v $STRIP_STATIC "$builddir/vmlinux"

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}

_package-docs() {
  pkgdesc="Documentation for the $pkgdesc kernel"

  cd $_srcname
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing documentation..."
  local src dst
  while read -rd '' src; do
    dst="${src#Documentation/}"
    dst="$builddir/Documentation/${dst#output/}"
    install -Dm644 "$src" "$dst"
  done < <(find Documentation -name '.*' -prune -o ! -type d -print0)

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/share/doc"
  ln -sr "$builddir/Documentation" "$pkgdir/usr/share/doc/$pkgbase"
}

pkgname=(
  "$pkgbase"
  "$pkgbase-headers"
  "$pkgbase-docs"
)
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#$pkgbase}
  }"
done

# vim:set ts=8 sts=2 sw=2 et:
